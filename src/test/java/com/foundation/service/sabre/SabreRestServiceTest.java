package com.foundation.service.sabre;

import static org.junit.Assert.assertNotNull;

import com.foundation.domain.model.sabre.FareForecastResponse;
import com.foundation.domain.model.sabre.TopDestinationsThemedResponse;
//import com.sun.org.apache.xerces.internal.impl.dv.util.Base64;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.Arrays;
import java.util.List;
import javax.imageio.ImageIO;
//import org.apache.commons.io.FileUtils;
import org.springframework.http.HttpEntity;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import org.springframework.beans.factory.annotation.Value;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import static org.junit.Assert.assertEquals;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

public class SabreRestServiceTest {
    @Test
    public void testForecastService() {
        RestTemplate restTemplate = new RestTemplate();
        
        String serviceUrl = "https://api.test.sabre.com/v1/forecast/flights/fares?origin=DFW&destination=LAX&departuredate=2016-06-01&returndate=2016-06-05";
        
        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", "Bearer T1RLAQLMA+OGPfYtT7iMvUJTBQ2jtE0ZkhDIKMy+skdK1hSfJp2hiEKxAACgp6eNMwmdtQA7O5jfSpH711ZZOBjHInwsSVRgwugq5Vgd0r5eK+/zHIWZS7ajvtaWuP/Lq5zGcXoDAuczN8RAFgg7wPgfGZm32MOvU/dHlkoo5gwCW9Pm17QK5r7woJlhRoP3/2eLkWSS+LV3pOkc0fYt25yN0JVw94REikZrzyG204Z86uey7RM9x31YJJYNKXdsb7HG2R0IQLiIEkT6Ww**");
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        
        HttpEntity<String> entity = new HttpEntity<String>("parameters", headers);
        
        ResponseEntity<FareForecastResponse> fareFcstResponse = restTemplate.exchange(serviceUrl, HttpMethod.GET, entity, FareForecastResponse.class);
        assertNotNull(fareFcstResponse);
        System.out.println("r:" +fareFcstResponse.getBody());
        System.out.println("response:" + fareFcstResponse.toString());
        

    }
    
    @Test
    public void testTopDestinationService() {
        RestTemplate restTemplate = new RestTemplate();
        
        //String serviceUrl = "https://api.test.sabre.com/v1/forecast/flights/fares?origin=DFW&destination=LAX&departuredate=2016-06-01&returndate=2016-06-05";
        String serviceUrl = "https://api.test.sabre.com/v1/lists/top/destinations?origin=DFW&lookbackweeks=8&topdestinations=20&theme=BEACH";
        
        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", "Bearer T1RLAQLMA+OGPfYtT7iMvUJTBQ2jtE0ZkhDIKMy+skdK1hSfJp2hiEKxAACgp6eNMwmdtQA7O5jfSpH711ZZOBjHInwsSVRgwugq5Vgd0r5eK+/zHIWZS7ajvtaWuP/Lq5zGcXoDAuczN8RAFgg7wPgfGZm32MOvU/dHlkoo5gwCW9Pm17QK5r7woJlhRoP3/2eLkWSS+LV3pOkc0fYt25yN0JVw94REikZrzyG204Z86uey7RM9x31YJJYNKXdsb7HG2R0IQLiIEkT6Ww**");
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        
        HttpEntity<String> entity = new HttpEntity<String>("parameters", headers);
        
        ResponseEntity<TopDestinationsThemedResponse> destnResponse = restTemplate.exchange(serviceUrl, HttpMethod.GET, entity, TopDestinationsThemedResponse.class);
        assertNotNull(destnResponse);
        System.out.println("r:" +destnResponse.getBody());
        System.out.println("response:" + destnResponse.toString());
        

    }
}
