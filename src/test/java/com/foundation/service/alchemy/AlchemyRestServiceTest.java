package com.foundation.service.alchemy;

import static org.junit.Assert.assertNotNull;

import com.foundation.domain.model.alchemy.*;
//import com.sun.org.apache.xerces.internal.impl.dv.util.Base64;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.Arrays;
import java.util.List;
import javax.imageio.ImageIO;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.io.FileUtils;
//import org.apache.commons.io.FileUtils;
import org.springframework.http.HttpEntity;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import org.springframework.beans.factory.annotation.Value;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import static org.junit.Assert.assertEquals;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

public class AlchemyRestServiceTest {
    @Test
    public void testImageTagService() throws IOException {
        System.out.println("Inside imageTagService");
        RestTemplate restTemplate = new RestTemplate();
        String serviceUrl = "https://gateway-a.watsonplatform.net/calls/images/ImageGetRankedImageKeywords?outputMode=json&imagePostMode=raw&apikey=cc92b43da923eb606d94e475ab66a188e8731462";
        
        /*HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        headers.setContentType(MediaType.APPLICATION_JSON);*/
        
        
        HttpHeaders headers = new HttpHeaders();
        //headers.add("Authorization", "Basic VmpFNk9HeGxlbU5oWVhaMmN6UnhjRGh2Y0RwRVJWWkRSVTVVUlZJNlJWaFU6UjJ3NGNqRlhRV3c9");
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);

        //HttpEntity<String> entity = new HttpEntity<String>("parameters", headers);
        // Create the request body as a MultiValueMap
        MultiValueMap<String, String> body = new LinkedMultiValueMap<String, String>();     
        /*body.add("imagePostMode", "raw");
        body.add("apiKey", "cc92b43da923eb606d94e475ab66a188e8731462");
        body.add("outputMode", "json");*/
        
        //File f = new File("C:\\img\\viceroy-bali.jpg");
        /*File f = new File("C:\\img\\oahu.jpg");
        byte[] img = Files.readAllBytes(f.toPath());*/
        
        /*File f=new File("C:\\img\\oahu.jpg");
        BufferedImage originalImage=ImageIO.read(f);
        ByteArrayOutputStream baos=new ByteArrayOutputStream();
        ImageIO.write(originalImage, "jpg", baos );
        byte[] img=baos.toByteArray();*/
        
        //body.add("img_file", img);
        //body.add("img_file", img.toString());
        //body.add("img_file", Base64.encode(FileUtils.readFileToByteArray(new File("C:\\img\\oahu.jpg"))));
        body.add("img_file", Base64.encodeBase64String(FileUtils.readFileToByteArray(new File("C:\\img\\oahu.jpg"))));
        
        
        // Note the body object as first parameter!
        HttpEntity<?> entity = new HttpEntity<Object>(body, headers);

        
        
        // Add the Jackson message converter
        /*List<HttpMessageConverter<?>> msgConverters = restTemplate.getMessageConverters();
        msgConverters.add(new MappingJackson2HttpMessageConverter());
        restTemplate.setMessageConverters(msgConverters);*/
        
        List<HttpMessageConverter<?>> messageConverters = restTemplate.getMessageConverters();
        for(HttpMessageConverter httpMessageConverter : messageConverters){
          System.out.println(httpMessageConverter);
        }
        
        //ResponseEntity<FareForecastResponse> fareFcstResponse = restTemplate.getForEntity(serviceUrl, FareForecastResponse.class);
        //ResponseEntity<FareForecastResponse> fareFcstResponse = restTemplate.exchange(serviceUrl, HttpMethod.GET, entity, FareForecastResponse.class);
        ResponseEntity<RankedImageResponse> response = restTemplate.exchange(serviceUrl, HttpMethod.POST, entity, RankedImageResponse.class);
        assertNotNull(response);
        System.out.println("imgResponse:" + response.getBody().toString());
        
        
        //Boolean status = restTemplate.postForObject(serviceUrl, request, Boolean.class);
        //log.info("Finished addEmployeeEligibilityDetails with status " + status);
    }
    
    @Test
    public void testImageTagServiceByUrl() throws IOException {
        System.out.println("Inside imageTagService");
        RestTemplate restTemplate = new RestTemplate();
        String imageUrl = "http://www.hiltonhawaiianvillage.com/assets/img/discover/oahu-island-activities/HHV_Oahu-island-activities_Content_Beaches_455x248_x2.jpg";
        //String imageUrl = "http://www.edisonmuckers.org/fun-facts-about-tom/";
        //String serviceUrl = "https://gateway-a.watsonplatform.net/calls/images/ImageGetRankedImageKeywords?outputMode=json&imagePostMode=raw&apikey=cc92b43da923eb606d94e475ab66a188e8731462";
        String serviceUrl = "https://gateway-a.watsonplatform.net/calls/url/URLGetRankedImageKeywords?url="+imageUrl+"&outputMode=json&apikey=cc92b43da923eb606d94e475ab66a188e8731462";
        
        /*HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        headers.setContentType(MediaType.APPLICATION_JSON);*/
        
        
        HttpHeaders headers = new HttpHeaders();
        //headers.add("Authorization", "Basic VmpFNk9HeGxlbU5oWVhaMmN6UnhjRGh2Y0RwRVJWWkRSVTVVUlZJNlJWaFU6UjJ3NGNqRlhRV3c9");
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        //headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);

        //HttpEntity<String> entity = new HttpEntity<String>("parameters", headers);
        // Create the request body as a MultiValueMap
        MultiValueMap<String, String> body = new LinkedMultiValueMap<String, String>();     
        /*body.add("imagePostMode", "raw");
        body.add("apiKey", "cc92b43da923eb606d94e475ab66a188e8731462");
        body.add("outputMode", "json");*/
        
        //File f = new File("C:\\img\\viceroy-bali.jpg");
        /*File f = new File("C:\\img\\oahu.jpg");
        byte[] img = Files.readAllBytes(f.toPath());*/
        
        /*File f=new File("C:\\img\\oahu.jpg");
        BufferedImage originalImage=ImageIO.read(f);
        ByteArrayOutputStream baos=new ByteArrayOutputStream();
        ImageIO.write(originalImage, "jpg", baos );
        byte[] img=baos.toByteArray();*/
        
        //body.add("img_file", img);
        //body.add("img_file", img.toString());
        //body.add("img_file", Base64.encode(FileUtils.readFileToByteArray(new File("C:\\img\\oahu.jpg"))));
        
        
        // Note the body object as first parameter!
        HttpEntity<?> entity = new HttpEntity<Object>(body, headers);

        
        
        // Add the Jackson message converter
        /*List<HttpMessageConverter<?>> msgConverters = restTemplate.getMessageConverters();
        msgConverters.add(new MappingJackson2HttpMessageConverter());
        restTemplate.setMessageConverters(msgConverters);*/
        
        List<HttpMessageConverter<?>> messageConverters = restTemplate.getMessageConverters();
        for(HttpMessageConverter httpMessageConverter : messageConverters){
          System.out.println(httpMessageConverter);
        }
        
        //ResponseEntity<FareForecastResponse> fareFcstResponse = restTemplate.getForEntity(serviceUrl, FareForecastResponse.class);
        //ResponseEntity<FareForecastResponse> fareFcstResponse = restTemplate.exchange(serviceUrl, HttpMethod.GET, entity, FareForecastResponse.class);
        ResponseEntity<RankedImageResponse> response = restTemplate.exchange(serviceUrl, HttpMethod.GET, entity, RankedImageResponse.class);
        assertNotNull(response);
        System.out.println("imgResponse:" + response.getBody().toString());
        
        
        //Boolean status = restTemplate.postForObject(serviceUrl, request, Boolean.class);
        //log.info("Finished addEmployeeEligibilityDetails with status " + status);
    }
}
