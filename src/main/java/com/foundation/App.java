package com.foundation;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.context.web.SpringBootServletInitializer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;

import java.io.File;

/**
 * Created by albertvmanoharan on 4/14/16.
 */
@EnableAutoConfiguration
@ComponentScan("com.foundation")
public class App {

    //public static String ROOT = "/tmp/images/";


    public static void main(String[] args) throws Throwable {
        SpringApplication.run(App.class, args);
    }

    @Bean
    CommandLineRunner init() {
        return (String[] args) -> {
            //new File(ROOT).mkdir();
            System.out.println("Inside init");
        };
    }

}
