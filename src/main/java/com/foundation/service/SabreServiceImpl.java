package com.foundation.service;

import com.foundation.domain.model.alchemy.ImageKeywords;
import com.foundation.domain.model.sabre.DestinationResponse;
import com.foundation.domain.model.sabre.Destinations;
import com.foundation.domain.model.sabre.FareForecastResponse;
import com.foundation.domain.model.sabre.TopDestinationsThemedResponse;
import java.util.Arrays;
import java.util.List;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class SabreServiceImpl implements SabreService {

    @Override
    public Destinations getCitiesByTheme(String themeName) {
        System.out.println("getCitiesByTheme:"+themeName);
        RestTemplate restTemplate = new RestTemplate();
        
        String serviceUrl = "https://api.test.sabre.com/v1/lists/supported/shop/themes/" + themeName;
        
        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", "Bearer T1RLAQLMA+OGPfYtT7iMvUJTBQ2jtE0ZkhDIKMy+skdK1hSfJp2hiEKxAACgp6eNMwmdtQA7O5jfSpH711ZZOBjHInwsSVRgwugq5Vgd0r5eK+/zHIWZS7ajvtaWuP/Lq5zGcXoDAuczN8RAFgg7wPgfGZm32MOvU/dHlkoo5gwCW9Pm17QK5r7woJlhRoP3/2eLkWSS+LV3pOkc0fYt25yN0JVw94REikZrzyG204Z86uey7RM9x31YJJYNKXdsb7HG2R0IQLiIEkT6Ww**");
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        
        HttpEntity<String> entity = new HttpEntity<String>("parameters", headers);
        
        ResponseEntity<Destinations> destinationResponse = restTemplate.exchange(serviceUrl, HttpMethod.GET, entity, Destinations.class);
        System.out.println("r:" +destinationResponse.getBody());
        System.out.println("response:" + destinationResponse.toString());
        return destinationResponse.getBody();
    }

    @Override
    public TopDestinationsThemedResponse getTopDestinationsByTheme(String themeName) {
        System.out.println("getTopDestinationsByTheme:"+themeName);
        RestTemplate restTemplate = new RestTemplate();
        
        
        String serviceUrl = "https://api.test.sabre.com/v1/lists/top/destinations?origin=DFW&lookbackweeks=8&topdestinations=20&theme=" + themeName;
        System.out.println("SABRE API Call:" + serviceUrl);
        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", "Bearer T1RLAQLMA+OGPfYtT7iMvUJTBQ2jtE0ZkhDIKMy+skdK1hSfJp2hiEKxAACgp6eNMwmdtQA7O5jfSpH711ZZOBjHInwsSVRgwugq5Vgd0r5eK+/zHIWZS7ajvtaWuP/Lq5zGcXoDAuczN8RAFgg7wPgfGZm32MOvU/dHlkoo5gwCW9Pm17QK5r7woJlhRoP3/2eLkWSS+LV3pOkc0fYt25yN0JVw94REikZrzyG204Z86uey7RM9x31YJJYNKXdsb7HG2R0IQLiIEkT6Ww**");
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        
        HttpEntity<String> entity = new HttpEntity<String>("parameters", headers);
        
        ResponseEntity<TopDestinationsThemedResponse> destnResponse = restTemplate.exchange(serviceUrl, HttpMethod.GET, entity, TopDestinationsThemedResponse.class);
        System.out.println("r:" +destnResponse.getBody());
        System.out.println("response:" + destnResponse.toString());
        return destnResponse.getBody();
    }
    
    @Override
    public FareForecastResponse getFareForecast(String destinationName) {
        System.out.println("getFareForecast:"+destinationName);
        RestTemplate restTemplate = new RestTemplate();
        
        String serviceUrl = "https://api.test.sabre.com/v1/forecast/flights/fares?origin=DFW&departuredate=2016-06-01&returndate=2016-06-05&destination="+destinationName;
        
        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", "Bearer T1RLAQLMA+OGPfYtT7iMvUJTBQ2jtE0ZkhDIKMy+skdK1hSfJp2hiEKxAACgp6eNMwmdtQA7O5jfSpH711ZZOBjHInwsSVRgwugq5Vgd0r5eK+/zHIWZS7ajvtaWuP/Lq5zGcXoDAuczN8RAFgg7wPgfGZm32MOvU/dHlkoo5gwCW9Pm17QK5r7woJlhRoP3/2eLkWSS+LV3pOkc0fYt25yN0JVw94REikZrzyG204Z86uey7RM9x31YJJYNKXdsb7HG2R0IQLiIEkT6Ww**");
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        
        HttpEntity<String> entity = new HttpEntity<String>("parameters", headers);
        
        ResponseEntity<FareForecastResponse> fareFcstResponse = restTemplate.exchange(serviceUrl, HttpMethod.GET, entity, FareForecastResponse.class);
        System.out.println("r:" +fareFcstResponse.getBody());
        System.out.println("response:" + fareFcstResponse.toString());
        return fareFcstResponse.getBody();
    }
    
}
