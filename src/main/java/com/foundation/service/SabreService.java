package com.foundation.service;

import com.foundation.domain.model.alchemy.ImageKeywords;
import com.foundation.domain.model.sabre.DestinationResponse;
import com.foundation.domain.model.sabre.Destinations;
import com.foundation.domain.model.sabre.FareForecastResponse;
import com.foundation.domain.model.sabre.TopDestinationsThemedResponse;
import java.util.List;

public interface SabreService {
    public Destinations getCitiesByTheme(String themeName);
    public TopDestinationsThemedResponse getTopDestinationsByTheme(String themeName);
    public FareForecastResponse getFareForecast(String destinationName);
}
