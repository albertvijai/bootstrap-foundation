/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.foundation.service;

import com.foundation.domain.model.alchemy.ImageKeywords;
import com.foundation.domain.model.alchemy.RankedImageResponse;
import java.io.File;
import java.util.Arrays;
import java.util.List;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.util.StringUtils;
import org.springframework.web.client.RestTemplate;

@Service
public class AlchemyServiceImpl implements AlchemyService {

    @Value("${isStubbed}")
    private String isStubbed;
    
    @Value("${alchemyUrl}")
    private String alchemyUrl;
    
    /*@Value("${alchemyService}")
    private String alchemyService;*/
    
    @Value("${alchemyServiceNameByImage}")
    private String alchemyServiceNameByImage;
    
    @Value("${alchemyServiceNameByUrl}")
    private String alchemyServiceNameByUrl;
    
    @Value("${alchemyApiKey}")
    private String alchemyApiKey;
    
    @Value("${imageFilePathPrefix}")
    private String imageFilePathPrefix;
    
    @Value("${localIP}")
    private String localIP;
    
    String[] genres = {"ocean","mountain"};
    private List<String> genresList = Arrays.asList(genres);
    
    
    @Override
    public List<ImageKeywords> getRankedImageKeywordsByImage(String filePath) {
        boolean isStubbedBool = false;//By default use real-time implementation - TODO
        System.out.println("isStubbed:" + isStubbed);
        List<ImageKeywords> imageKeywords;
        if(StringUtils.isEmpty(isStubbed)) {
            isStubbedBool = true;
        } else {
            isStubbedBool = new Boolean(isStubbed);
        }
        System.out.println("isStubbedBool:" + isStubbedBool);
        if(isStubbedBool) {
            imageKeywords = returnStubbedResponseByImage();
        } else {
            String params = "outputMode=json&imagePostMode=raw";
            String url = createUrl(alchemyServiceNameByImage, params);
            
            RestTemplate restTemplate = new RestTemplate();
        
            HttpHeaders headers = new HttpHeaders();
            headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
            headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);

            //HttpEntity<String> entity = new HttpEntity<String>("parameters", headers);
            // Create the request body as a MultiValueMap
            MultiValueMap<String, String> body = new LinkedMultiValueMap<String, String>();     

            try {
                System.out.println("imageFilePathPrefix:" + imageFilePathPrefix);
                String imageFilePath = imageFilePathPrefix.concat(filePath).concat(".jpg");
                System.out.println("imageFilePath:" + imageFilePath);
                body.add("img_file", Base64.encodeBase64String(FileUtils.readFileToByteArray(new File(imageFilePath))));

                // Note the body object as first parameter!
                HttpEntity<?> entity = new HttpEntity<Object>(body, headers);
                ResponseEntity<RankedImageResponse> response = restTemplate.exchange(url, HttpMethod.POST, entity, RankedImageResponse.class);

                System.out.println("imgResponse:" + response.getBody().toString());
                //TODO Check for status code
                imageKeywords = response.getBody().getImageKeywords();
            } catch (Exception ex) {
                ex.printStackTrace();
                imageKeywords = returnStubbedResponseByImage();
            }
        }
        return imageKeywords;
    }
    
    private List<ImageKeywords> returnStubbedResponseByImage() {
        return null;
    }

    @Override
    public List<ImageKeywords> getRankedImageKeywordsByUrl(String imageName) {
        boolean isStubbedBool = false;//By default use stubbed implementation
        StringUtils.isEmpty("isStubbed:" + isStubbed);
        List<ImageKeywords> imageKeywords;
        if(StringUtils.isEmpty(isStubbed)) {
            isStubbedBool = true;
        }
        if(isStubbedBool) {
            imageKeywords = returnStubbedResponseByImage();
        } else {
            //String imageUrl = "http://www.hiltonhawaiianvillage.com/assets/img/discover/oahu-island-activities/HHV_Oahu-island-activities_Content_Beaches_455x248_x2.jpg";
            String imageUrl = localIP.concat("picture/").concat(imageName).concat(".jpg");
            System.out.println("imageUrl:" + imageUrl);
            String params = "outputMode=json&url="+imageUrl;
            String url = createUrl(alchemyServiceNameByUrl, params);
            
            RestTemplate restTemplate = new RestTemplate();
        
            HttpHeaders headers = new HttpHeaders();
            headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
            
            // Create the dummy request body as a MultiValueMap
            MultiValueMap<String, String> body = new LinkedMultiValueMap<String, String>();     

            try {
                
                HttpEntity<?> entity = new HttpEntity<Object>(body, headers);
                ResponseEntity<RankedImageResponse> response = restTemplate.exchange(url, HttpMethod.GET, entity, RankedImageResponse.class);

                System.out.println("imgResponse:" + response.getBody().toString());
                //TODO Check for status code
                imageKeywords = response.getBody().getImageKeywords();
            } catch (Exception ex) {
                ex.printStackTrace();
                imageKeywords = returnStubbedResponseByImage();
            }         
        }
        return imageKeywords;
        
    }
    
    private String createUrl(String serviceName, String params) {
        String url = alchemyUrl.concat(serviceName).concat("?").concat(params).concat("&").concat("apikey=").concat(alchemyApiKey);
        System.out.println("Returning URL:" + url);
        return url;
    }
}
