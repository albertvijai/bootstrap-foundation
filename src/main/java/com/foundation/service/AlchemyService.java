package com.foundation.service;

import com.foundation.domain.model.alchemy.ImageKeywords;
import java.util.List;

public interface AlchemyService {
    public List<ImageKeywords> getRankedImageKeywordsByImage(String filePath);
    public List<ImageKeywords> getRankedImageKeywordsByUrl(String imageName);
}
