package com.foundation.domain;

import org.springframework.data.annotation.Id;

/**
 * Created by albertvmanoharan on 4/14/16.
 */
public class Foundation {

    @Id
    private String id;

    private String firstName;
    private String lastName;

    public Foundation() {}

    public Foundation(String firstName, String lastName) {
        this.firstName = firstName;
        this.lastName = lastName;
    }

    @Override
    public String toString() {
        return String.format(
                "Foundation[id=%s, firstName='%s', lastName='%s']",
                id, firstName, lastName);
    }
}
