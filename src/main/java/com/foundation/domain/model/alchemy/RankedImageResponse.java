package com.foundation.domain.model.alchemy;

import java.util.List;

public class RankedImageResponse implements java.io.Serializable {
    String status;
    String statusInfo;
    String usage;
    String url;
    String totalTransactions;
    List<ImageKeywords> imageKeywords;

    public RankedImageResponse() {
    }

    
    
    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatusInfo() {
        return statusInfo;
    }

    public void setStatusInfo(String statusInfo) {
        this.statusInfo = statusInfo;
    }

    public String getUsage() {
        return usage;
    }

    public void setUsage(String usage) {
        this.usage = usage;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getTotalTransactions() {
        return totalTransactions;
    }

    public void setTotalTransactions(String totalTransactions) {
        this.totalTransactions = totalTransactions;
    }

    public List<ImageKeywords> getImageKeywords() {
        return imageKeywords;
    }

    public void setImageKeywords(List<ImageKeywords> imageKeywords) {
        this.imageKeywords = imageKeywords;
    }

    @Override
    public String toString() {
        return "RankedImageResponse{" + "status=" + status + ", statusInfo=" + statusInfo + ", usage=" + usage + ", url=" + url + ", totalTransactions=" + totalTransactions + ", imageKeywords=" + imageKeywords + '}';
    }   
    
}
