package com.foundation.domain.model.alchemy;

public class ImageKeywords implements java.io.Serializable {
    String text;
    String score;

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getScore() {
        return score;
    }

    public void setScore(String score) {
        this.score = score;
    }

    @Override
    public String toString() {
        return "ImageKeywords{" + "text=" + text + ", score=" + score + '}';
    }
    
    
}
