package com.foundation.domain.model.alchemy;

import java.util.ArrayList;
import java.util.List;

public class ImageKeywordsList implements java.io.Serializable {
    List<ImageKeywords> imageKeywords;

    public List<ImageKeywords> getImageKeywords() {
        if(imageKeywords == null) {
            imageKeywords = new ArrayList<ImageKeywords>();
        }
        return imageKeywords;
    }
}
