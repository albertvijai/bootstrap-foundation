/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.foundation.domain.model.sabre;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Links implements java.io.Serializable {
	@JsonProperty("rel")
        String rel;
        
        @JsonProperty("href")
	String href;

    public String getRel() {
        return rel;
    }

    public void setRel(String rel) {
        this.rel = rel;
    }

    public String getHref() {
        return href;
    }

    public void setHref(String href) {
        this.href = href;
    }

    @Override
    public String toString() {
        return "Links{" + "rel=" + rel + ", href=" + href + '}';
    }   
}

