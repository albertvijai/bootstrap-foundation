package com.foundation.domain.model.sabre;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.List;

public class Destination implements java.io.Serializable {
    @JsonProperty("DestinationLocation")
    private String destinationLocation;
    
    @JsonProperty("MetropolitanAreaName")
    private String metropolitanAreaName;
    
    @JsonProperty("AirportName")
    private String airportName;
    
    @JsonProperty("CityName")
    private String cityName;
    
    @JsonProperty("CountryCode")
    private String countryCode;
    
    @JsonProperty("CountryName")
    private String coutryName;
    
    @JsonProperty("RegionName")
    private String regionName;
    
    @JsonProperty("Type")
    private String type;
    
    @JsonProperty("Links")
    private List<Links> links;

    public String getDestinationLocation() {
        return destinationLocation;
    }

    public void setDestinationLocation(String destinationLocation) {
        this.destinationLocation = destinationLocation;
    }

    public String getAirportName() {
        return airportName;
    }

    public void setAirportName(String airportName) {
        this.airportName = airportName;
    }

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public String getCoutryName() {
        return coutryName;
    }

    public void setCoutryName(String coutryName) {
        this.coutryName = coutryName;
    }

    public String getRegionName() {
        return regionName;
    }

    public void setRegionName(String regionName) {
        this.regionName = regionName;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getMetropolitanAreaName() {
        return metropolitanAreaName;
    }

    public void setMetropolitanAreaName(String metropolitanAreaName) {
        this.metropolitanAreaName = metropolitanAreaName;
    }

    public List<Links> getLinks() {
        return links;
    }

    public void setLinks(List<Links> links) {
        this.links = links;
    }
}
