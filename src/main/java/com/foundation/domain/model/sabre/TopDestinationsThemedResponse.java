package com.foundation.domain.model.sabre;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.List;

public class TopDestinationsThemedResponse implements java.io.Serializable {
    @JsonProperty("Theme")
    String theme;
    
    @JsonProperty("OriginLocation")
    String originLocation;
    
    @JsonProperty("Destinations")
    List<RankedDestination> destinations;
    
    @JsonProperty("LookBackWeeks")
    String lookBackWeeks;
    
    @JsonProperty("Links")
    //@JsonIgnore
    List<Links> links;

    public TopDestinationsThemedResponse() {
        System.out.println("Inside TopDestinationsThemedResponse");
    }
    
    

    public String getTheme() {
        return theme;
    }

    public void setTheme(String theme) {
        this.theme = theme;
    }

    public String getOriginLocation() {
        return originLocation;
    }

    public void setOriginLocation(String originLocation) {
        this.originLocation = originLocation;
    }

    public List<RankedDestination> getDestinations() {
        return destinations;
    }

    public void setDestinations(List<RankedDestination> destinations) {
        this.destinations = destinations;
    }

    public String getLookBackWeeks() {
        return lookBackWeeks;
    }

    public void setLookBackWeeks(String lookBackWeeks) {
        this.lookBackWeeks = lookBackWeeks;
    }

    public List<Links> getLinks() {
        return links;
    }

    public void setLinks(List<Links> links) {
        this.links = links;
    }
}
