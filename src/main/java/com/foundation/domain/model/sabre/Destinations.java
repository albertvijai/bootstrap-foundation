package com.foundation.domain.model.sabre;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.List;

public class Destinations implements java.io.Serializable {
    @JsonProperty("Destinations")
    List<DestinationResponse> destinationResponse;
    
    @JsonProperty("Links")
    List<Links> links;

    public List<DestinationResponse> getDestinationResponse() {
        return destinationResponse;
    }

    public void setDestinationResponse(List<DestinationResponse> destinationResponse) {
        this.destinationResponse = destinationResponse;
    }

    public List<Links> getLinks() {
        return links;
    }

    public void setLinks(List<Links> links) {
        this.links = links;
    }
}
