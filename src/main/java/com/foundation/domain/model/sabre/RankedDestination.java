package com.foundation.domain.model.sabre;

import com.fasterxml.jackson.annotation.JsonProperty;

public class RankedDestination implements java.io.Serializable {
    @JsonProperty("Rank")
    String rank;
    
    @JsonProperty("Destination")
    Destination destination;

    public String getRank() {
        return rank;
    }

    public void setRank(String rank) {
        this.rank = rank;
    }

    public Destination getDestination() {
        return destination;
    }

    public void setDestination(Destination destination) {
        this.destination = destination;
    }
}
