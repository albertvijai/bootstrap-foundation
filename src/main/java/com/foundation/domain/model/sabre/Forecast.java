package com.foundation.domain.model.sabre;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.io.Serializable;

public class Forecast implements Serializable {
	@JsonProperty("HighestPredictedFare")
        String highestPredictedFare;
        
        @JsonProperty("CurrencyCode")
	String currencyCode;
        
        @JsonProperty("LowestPredictedFare")
	String lowestPredictedFare;

    public String getHighestPredictedFare() {
        return highestPredictedFare;
    }

    public void setHighestPredictedFare(String highestPredictedFare) {
        this.highestPredictedFare = highestPredictedFare;
    }

    public String getCurrencyCode() {
        return currencyCode;
    }

    public void setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
    }

    public String getLowestPredictedFare() {
        return lowestPredictedFare;
    }

    public void setLowestPredictedFare(String lowestPredictedFare) {
        this.lowestPredictedFare = lowestPredictedFare;
    }

    @Override
    public String toString() {
        return "Forecast{" + "highestPredictedFare=" + highestPredictedFare + ", currencyCode=" + currencyCode + ", lowestPredictedFare=" + lowestPredictedFare + '}';
    }
        
    
        
}

