package com.foundation.domain.model.sabre;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.List;

public class FareForecastResponse implements java.io.Serializable {
	@JsonProperty("OriginLocation")
        String originLocation;
        
        @JsonProperty("DestinationLocation")
	String destinationLocation;
	
        @JsonProperty("DepartureDateTime")
        String departureDateTime;
        
        @JsonProperty("ReturnDateTime")
	String returnDateTime;
	
        @JsonProperty("Forecast")
        Forecast forecast;
        
        @JsonProperty("Recommendation")
	String recommendation;
        
        @JsonProperty("LowestFare")
	String lowestFare;
        
        @JsonProperty("CurrencyCode")
	String currencyCode;
	
        @JsonProperty("Links")
        List<Links> links;

    public FareForecastResponse() {
        System.out.println("Inside constructor");
    }
        
        

    public String getOriginLocation() {
        return originLocation;
    }

    public void setOriginLocation(String originLocation) {
        System.out.println("Inside setOrigin:" + originLocation);
        this.originLocation = originLocation;
    }

    public String getDestinationLocation() {
        return destinationLocation;
    }

    public void setDestinationLocation(String destinationLocation) {
        this.destinationLocation = destinationLocation;
    }

    public String getDepartureDateTime() {
        return departureDateTime;
    }

    public void setDepartureDateTime(String departureDateTime) {
        this.departureDateTime = departureDateTime;
    }

    public String getReturnDateTime() {
        return returnDateTime;
    }

    public void setReturnDateTime(String returnDateTime) {
        this.returnDateTime = returnDateTime;
    }

    public Forecast getForecast() {
        return forecast;
    }

    public void setForecast(Forecast forecast) {
        this.forecast = forecast;
    }

    public String getRecommendation() {
        return recommendation;
    }

    public void setRecommendation(String recommendation) {
        this.recommendation = recommendation;
    }

    public String getLowestFare() {
        return lowestFare;
    }

    public void setLowestFare(String lowestFare) {
        this.lowestFare = lowestFare;
    }

    public String getCurrencyCode() {
        return currencyCode;
    }

    public void setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
    }

    public List<Links> getLinks() {
        return links;
    }

    public void setLinks(List<Links> links) {
        this.links = links;
    }

    /*@Override
    public String toString() {
        return "FareForecastResponse{" + "originLocation=" + originLocation + ", destinationLocation=" + destinationLocation + ", departureDateTime=" + departureDateTime + ", returnDateTime=" + returnDateTime + ", forecast=" + forecast + ", recommendation=" + recommendation + ", lowestFare=" + lowestFare + ", currencyCode=" + currencyCode + ", links=" + links + '}';
    }*/

    /*@Override
    public String toString() {
        return "FareForecastResponse{" + "originLocation=" + originLocation + ", destinationLocation=" + destinationLocation + ", departureDateTime=" + departureDateTime + ", returnDateTime=" + returnDateTime + ", recommendation=" + recommendation + ", lowestFare=" + lowestFare + ", currencyCode=" + currencyCode + '}';
    }*/

    @Override
    public String toString() {
        return "FareForecastResponse{" + "originLocation=" + originLocation + ", destinationLocation=" + destinationLocation + ", departureDateTime=" + departureDateTime + ", returnDateTime=" + returnDateTime + ", forecast=" + forecast + ", recommendation=" + recommendation + ", lowestFare=" + lowestFare + ", currencyCode=" + currencyCode + ", links=" + links + '}';
    }

    

    

    
}

