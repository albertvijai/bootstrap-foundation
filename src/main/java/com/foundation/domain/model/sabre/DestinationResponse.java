package com.foundation.domain.model.sabre;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.List;

public class DestinationResponse implements java.io.Serializable {
    @JsonProperty("Destination")
    private String destination;
    
    @JsonProperty("Type")
    private String type;
    
    @JsonProperty("Links")
    List<Links> links;

    public DestinationResponse() {
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public List<Links> getLinks() {
        return links;
    }

    public void setLinks(List<Links> links) {
        this.links = links;
    }

    @Override
    public String toString() {
        return "DestinationResponse{" + "destination=" + destination + ", type=" + type + ", links=" + links + '}';
    }
}
