/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.foundation.web;

import com.foundation.domain.model.alchemy.ImageKeywords;
import com.foundation.domain.model.alchemy.ImageKeywordsList;
import com.foundation.service.AlchemyService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

@RestController
public class AlchemyController {
    
    @Autowired AlchemyService alchemyService;
    
    @RequestMapping(value = "/alchemy/getRankedImageKeywordsByImage/{filePath}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ImageKeywordsList> getRankedImageKeywordsByImage(@PathVariable("filePath") String filePath) {
        System.out.println("Fetching getRankedImageKeywordsByImage at path: " + filePath);
        List<ImageKeywords> imgKeywords = alchemyService.getRankedImageKeywordsByImage(filePath);
        ImageKeywordsList imgKwLi = new ImageKeywordsList();
        if (imgKeywords != null) {
            System.out.println("Found: " + imgKeywords.size() + " genres");
        }
        imgKwLi.getImageKeywords().addAll(imgKeywords);
        return new ResponseEntity<ImageKeywordsList>(imgKwLi, HttpStatus.OK);
    }
    
    @RequestMapping(value = "/alchemy/getRankedImageKeywordsByUrl/{imageName}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ImageKeywordsList> getRankedImageKeywordsByUrl(@PathVariable("imageName") String imageName) {
        System.out.println("Fetching getRankedImageKeywordsByUrl with name: " + imageName);
        //imageName = "http://www.edisonmuckers.org/wp-content/uploads/2014/02/unnamed.jpg";
        
        List<ImageKeywords> imgKeywords = alchemyService.getRankedImageKeywordsByUrl(imageName);
        ImageKeywordsList imgKwLi = new ImageKeywordsList();
        if (imgKeywords != null) {
            System.out.println("Found: " + imgKeywords.size() + " genres");
        }
        imgKwLi.getImageKeywords().addAll(imgKeywords);
        return new ResponseEntity<ImageKeywordsList>(imgKwLi, HttpStatus.OK);
    }
}
