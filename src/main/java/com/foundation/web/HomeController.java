package com.foundation.web;

import java.io.File;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Created by albertvmanoharan on 4/14/16.
 */

@Controller
public class HomeController {
    
    @Value("${imageFilePathPrefix}")
    private String imageFilePathPrefix;

    @RequestMapping("/index")
    public String index() {
        return "index";
    }


    @RequestMapping("/drop")
    public String init() {
        return "init";
    }
    


    @RequestMapping("/dnd")
    public String dnd() {
        return "dndImage";
    }
    
    @RequestMapping("/picture/{name}")
    @ResponseBody
    public HttpEntity<byte[]> getPicture(@PathVariable String name) {

        System.out.println("Requested picture >> " + name + " <<");
        
        System.out.println("imageFilePathPrefix:" + imageFilePathPrefix);
        String imageFilePath = imageFilePathPrefix.concat(name).concat(".jpg");
        System.out.println("imageFilePath:" + imageFilePath);
        

        // 1. download img from http://internal-picture-db/id.jpg ... 
        byte[] image = null;
        
        try {
            image = FileUtils.readFileToByteArray(new File(imageFilePath));
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.IMAGE_JPEG);
        headers.setContentLength(image.length);

        return new HttpEntity<byte[]>(image, headers);
    }





    //JUST DO IT ALBERT
    @RequestMapping("/landing")
    public String landing() {
        return "landing";
    }



}
