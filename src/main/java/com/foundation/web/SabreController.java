package com.foundation.web;

import com.foundation.domain.model.alchemy.ImageKeywords;
import com.foundation.domain.model.alchemy.ImageKeywordsList;
import com.foundation.domain.model.sabre.Destinations;
import com.foundation.domain.model.sabre.FareForecastResponse;
import com.foundation.domain.model.sabre.TopDestinationsThemedResponse;
import com.foundation.service.AlchemyService;
import com.foundation.service.SabreService;
import java.util.Arrays;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

@RestController
public class SabreController {
    @Autowired SabreService sabreService;
    
    private static final String BEACH_THEME = "BEACH";
    private static final String MOUNTAIN_THEME = "MOUNTAINS";
    private static final String SKIING_THEME = "SKIING";
    private static final String DISNEY_THEME = "DISNEY";
    
    String[] beachThemes = {"beach","sand","sea","island"};
    List<String> beachThemesLi = Arrays.asList(beachThemes);
    
    String[] mountainThemes = {"mountain","nature"};
    List<String> mountainThemesLi = Arrays.asList(mountainThemes);
    
    String[] skiingThemes = {"ice","snow","skiing","snowboarding"};
    List<String> skiingThemesLi = Arrays.asList(skiingThemes);
    
    String[] disneyThemes = {"disney","magic","kingdom","magic kingdom","cinderella"};
    List<String> disneyThemesLi = Arrays.asList(disneyThemes);
    
    @RequestMapping(value = "/sabre/getCitiesByTheme/{themeName}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Destinations> getCitiesByTheme(@PathVariable("themeName") String themeName) {
        System.out.println("Fetching getCitiesByTheme for theme: " + themeName);
        if(beachThemesLi.contains(themeName.toLowerCase())) {
            themeName = BEACH_THEME;
        } else if(mountainThemesLi.contains(themeName.toLowerCase())) {
            themeName = MOUNTAIN_THEME;
        } else if(skiingThemesLi.contains(themeName.toLowerCase())) {
            themeName = SKIING_THEME;
        } else if(disneyThemesLi.contains(themeName.toLowerCase())) {
            themeName = DISNEY_THEME;
        }
        
        Destinations destinations = sabreService.getCitiesByTheme(themeName);
        if (destinations != null && destinations.getDestinationResponse() != null) {
            System.out.println("Found: " + destinations.getDestinationResponse().size() + " destinations");
        }
        return new ResponseEntity<Destinations>(destinations, HttpStatus.OK);
    }
    
    @RequestMapping(value = "/sabre/getTopDestinationsByTheme/{themeName}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<TopDestinationsThemedResponse> getTopDestinationsByTheme(@PathVariable("themeName") String themeName) {
        System.out.println("Fetching getTopDestinationsByTheme for theme: " + themeName);
        if(beachThemesLi.contains(themeName.toLowerCase())) {
            themeName = BEACH_THEME;
        } else if(mountainThemesLi.contains(themeName.toLowerCase())) {
            themeName = MOUNTAIN_THEME;
        } else if(skiingThemesLi.contains(themeName.toLowerCase())) {
            themeName = SKIING_THEME;
        } else if(disneyThemesLi.contains(themeName.toLowerCase())) {
            themeName = DISNEY_THEME;
        }
        
        TopDestinationsThemedResponse destinations = sabreService.getTopDestinationsByTheme(themeName);
        if (destinations != null && destinations.getDestinations() != null) {
            System.out.println("Found: " + destinations.getDestinations().size() + " destinations");
        }
        return new ResponseEntity<TopDestinationsThemedResponse>(destinations, HttpStatus.OK);
    }
    
    @RequestMapping(value = "/sabre/getFareForecastByDestination/{destinationName}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<FareForecastResponse> getFareForecastByDestination(@PathVariable("destinationName") String destinationName) {
        System.out.println("Fetching getFareForecastByDestination for destination: " + destinationName);
        
        FareForecastResponse fareForecast = sabreService.getFareForecast(destinationName);
        if (fareForecast != null && fareForecast.getRecommendation()!= null) {
            System.out.println("Forecast for : " + destinationName + " is: " + fareForecast);
        }
        return new ResponseEntity<FareForecastResponse>(fareForecast, HttpStatus.OK);
    }
}
