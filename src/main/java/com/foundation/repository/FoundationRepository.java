package com.foundation.repository;

import com.foundation.domain.Foundation;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

/**
 * Created by albertvmanoharan on 4/14/16.
 */
public interface FoundationRepository extends MongoRepository<Foundation, String> {

    public Foundation findByFirstName(String firstName);
    public List<Foundation> findByLastName(String lastName);
}
